package daniel.myinstagallery;

/**
 * Created by Daniel on 04/11/2017.
 */

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;


public final class MyInstaGallery extends Application implements Application.ActivityLifecycleCallbacks {
    public static final String TAG = MyInstaGallery.class.getSimpleName();

    public static final String APP_NAME = "MyInstaGallery";

    public static final String CLIENT_ID = "9d82d15def8b4d138f88e5ca76928cbb";
    public static final String BASE_URL ="https://api.instagram.com/";
    public static final String REDIRECT_URI = "https://instagram.com/";

    public static final String END_POINT = MyInstaGallery.BASE_URL
            + "oauth/authorize/?client_id="
            + MyInstaGallery.CLIENT_ID
            + "&redirect_uri="
            + MyInstaGallery.REDIRECT_URI
            + "&response_type=token"
            + "&scope=basic"
            + "&display=touch";


    public static Context context;

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        super.onCreate();

        context = getApplicationContext();

        registerActivityLifecycleCallbacks(this);
    }

    //region Activity Lifecycle callbacks
    @Override
    public void onActivityCreated(@NonNull Activity activity, Bundle savedInstanceState) {
        Log.v(TAG, "onActivityCreated: " + activity);
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Log.v(TAG, "onActivityStarted: " + activity);
    }

    @Override
    public void onActivityResumed(Activity activity) {
        Log.v(TAG, "onActivityResumed: " + activity);
    }

    @Override
    public void onActivityPaused(Activity activity) {
        Log.v(TAG, "onActivityPaused: " + activity);
    }

    @Override
    public void onActivityStopped(Activity activity) {
        Log.v(TAG, "onActivityStopped: " + activity);
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        Log.v(TAG, "onActivitySaveInstanceState: " + activity);
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Log.v(TAG, "onActivityDestroyed: " + activity);
    }
    //end region
    
    public static boolean networkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }
}
