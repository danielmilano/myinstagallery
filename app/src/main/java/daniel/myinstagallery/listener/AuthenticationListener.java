package daniel.myinstagallery.listener;

/**
 * Created by Daniel on 05/11/2017.
 */

public interface AuthenticationListener {
    void onCodeReceived(String authToken);
}
