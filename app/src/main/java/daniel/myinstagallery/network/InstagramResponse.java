package daniel.myinstagallery.network;

/**
 * Created by Daniel on 05/11/2017.
 */
public class InstagramResponse {

    private Data[] data;

    public Data[] getData() {
        return data;
    }

    public void setData(Data[] data) {
        this.data = data;
    }
}