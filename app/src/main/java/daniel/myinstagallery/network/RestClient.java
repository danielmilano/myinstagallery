package daniel.myinstagallery.network;

import daniel.myinstagallery.MyInstaGallery;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Daniel on 05/11/2017.
 */

public class RestClient {

    public static RestService getRestService() {
        return new Retrofit.Builder()
                .baseUrl(MyInstaGallery.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(RestService.class);
    }
}