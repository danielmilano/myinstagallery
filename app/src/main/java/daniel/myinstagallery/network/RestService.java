package daniel.myinstagallery.network;

/**
 * Created by Daniel on 05/11/2017.
 */

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestService {

    @GET("v1/tags/{tag-name}/media/recent")
    Call<InstagramResponse> getTagPhotos(@Path("tag-name") String tag_name, @Query("access_token") String access_token);

    @GET("v1/users/self")
    Call<InstagramResponse> getUserInfo(@Query("access_token") String access_token);

    @GET("v1/users/self/media/recent")
    Call<InstagramResponse> getUserMedia(@Query("access_token") String access_token);
}

