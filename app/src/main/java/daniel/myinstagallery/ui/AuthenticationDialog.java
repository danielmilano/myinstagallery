package daniel.myinstagallery.ui;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Window;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import daniel.myinstagallery.MyInstaGallery;
import daniel.myinstagallery.R;
import daniel.myinstagallery.listener.AuthenticationListener;

/**
 * Created by Daniel on 05/11/2017.
 */

public class AuthenticationDialog extends Dialog {

    private Context context;
    private final AuthenticationListener listener;

    private WebView webView;
    private ProgressDialog mSpinner;

    private String accessToken;

    public AuthenticationDialog(@NonNull Context context, AuthenticationListener listener) {
        super(context);
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.auth_dialog);

        mSpinner = new ProgressDialog(getContext());
        mSpinner.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mSpinner.setMessage("Loading...");

        initializeWebView();
    }

    private void initializeWebView() {
        webView = (WebView) findViewById(R.id.web_view);
        webView.loadUrl(MyInstaGallery.END_POINT);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mSpinner.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                if (url.contains("#access_token=")) {
                    Uri uri = Uri.parse(url);
                    accessToken = uri.getEncodedFragment();
                    accessToken = accessToken.substring(accessToken.lastIndexOf("=")+1);
                    listener.onCodeReceived(accessToken);
                    dismiss();
                }
                else {
                    Toast.makeText(context, getContext().getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                }

                mSpinner.dismiss();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request){
                return false; // then it is not handled by default action
            }
        });
    }

    @Override
    public void setOnCancelListener(@Nullable OnCancelListener listener) {
        super.setOnCancelListener(listener);
    }
}