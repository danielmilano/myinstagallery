package daniel.myinstagallery.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import daniel.myinstagallery.R;
import daniel.myinstagallery.utils.FileUtils;

public class DetailActivity extends AppCompatActivity {

    private String currentUrl;
    private int currentPosition;

    private ArrayList<String> urlImages = new ArrayList<>();
    private Toolbar toolbar;
    private ViewPager viewPager;

    ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_pager);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setupActionBar();

        if (savedInstanceState != null) {
            currentUrl = savedInstanceState.getString(FeedActivity.EXTRA_URL_IMAGE);
            currentPosition = savedInstanceState.getInt(FeedActivity.EXTRA_URL_POSITION);
            urlImages = savedInstanceState.getStringArrayList(FeedActivity.EXTRA_URL_IMAGES_GALLERY);
        }

        Intent intent = getIntent();
        if (intent != null) {
            currentUrl = intent.getStringExtra(FeedActivity.EXTRA_URL_IMAGE);
            currentPosition = intent.getIntExtra(FeedActivity.EXTRA_URL_POSITION, 0);
            urlImages = intent.getStringArrayListExtra(FeedActivity.EXTRA_URL_IMAGES_GALLERY);
        }

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        viewPagerAdapter = new ViewPagerAdapter(DetailActivity.this, urlImages);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setCurrentItem(currentPosition); //REMIND: current item can be specified only after adapter has been set
    }

    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.a_menu_detail, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(@NonNull Menu menu) {
        menu.findItem(R.id.download).setVisible(true);
        menu.findItem(R.id.share).setVisible(true);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        currentUrl = urlImages.get(viewPager.getCurrentItem());
        switch (item.getItemId()) {
            case R.id.download:
                FileUtils.saveImage(currentUrl);
                break;
            case R.id.share:
                Intent share = FileUtils.shareImage(currentUrl);
                startActivity(Intent.createChooser(share, "Share Image"));
                break;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void setupActionBar() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    //Implement PagerAdapter Class to handle individual page creation
    class ViewPagerAdapter extends PagerAdapter {

        private Context context;
        private ArrayList<String> urlImages = new ArrayList<>();
        private String currentUrl;

        private TouchImageView image;

        public ViewPagerAdapter(Context context, ArrayList<String> urlImages) {
            this.context = context;
            this.urlImages = urlImages;
        }

        @Override
        public int getCount() {
            //Return total pages, here one for each data item
            return urlImages.size();
        }

        //Create the given page (indicated by position)
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LayoutInflater inflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.activity_detail, null);
            image = (TouchImageView) view.findViewById(R.id.image_detail);
            container.addView(view);

            currentUrl = urlImages.get(position);

            Picasso.with(context)
                    .load(currentUrl)
                    .into(image);
            return view;
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            //See if object from instantiateItem is related to the given view
            //required by API
            return arg0 == arg1;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }
}
