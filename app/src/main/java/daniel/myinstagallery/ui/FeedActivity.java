package daniel.myinstagallery.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import daniel.myinstagallery.MyInstaGallery;
import daniel.myinstagallery.R;
import daniel.myinstagallery.adapter.ListViewAdapter;
import daniel.myinstagallery.network.Data;
import daniel.myinstagallery.network.InstagramResponse;
import daniel.myinstagallery.network.RestClient;
import daniel.myinstagallery.utils.FileUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Daniel on 05/11/2017.
 */

public class FeedActivity extends AppCompatActivity {
    private static final String TAG = FeedActivity.class.getSimpleName();

    public static final String EXTRA_URL_IMAGE = "urlImage";
    public static final String EXTRA_URL_POSITION = "urlPosition";
    public static final String EXTRA_URL_IMAGES_GALLERY = "urlImagesGallery";

    private ListView listView;
    private ListViewAdapter listViewAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    private ArrayList<Data> data = new ArrayList<>();
    private ArrayList<String> urlImages = new ArrayList<>();

    private String accessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        // Get the accessToken from the intent extra
        Intent i = this.getIntent();
        accessToken = i.getStringExtra(LoginActivity.EXTRA_ACCESS_TOKEN);

        listView = (ListView) findViewById(R.id.lv_feed);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);

        // Set the listview adapter
        listViewAdapter = new ListViewAdapter(this, 0, data);
        listView.setAdapter(listViewAdapter);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                CharSequence choices[] = new CharSequence[]{"Share", "Save"};

                AlertDialog.Builder builder = new AlertDialog.Builder(FeedActivity.this);
                builder.setItems(choices, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (isStoragePermissionGranted()) {
                            final String url = listViewAdapter.getItem(position).getImages().getStandard_resolution().getUrl();
                            if (which == 0) {
                                Intent share = FileUtils.shareImage(url);
                                startActivity(Intent.createChooser(share, "Share Image"));
                            } else if (which == 1) {
                                FileUtils.saveImage(url);
                            }
                        }
                    }
                });
                builder.show();
                return true;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //REMIND: open image in full screen, action bar with "save", "share" and "details"
                String url = listViewAdapter.getItem(position).getImages().getStandard_resolution().getUrl();
                Intent i = new Intent(FeedActivity.this, DetailActivity.class);
                i.putExtra(EXTRA_URL_IMAGE, url);
                i.putExtra(EXTRA_URL_POSITION, position);
                i.putExtra(EXTRA_URL_IMAGES_GALLERY, urlImages);
                startActivity(i);
            }
        });

        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        Log.i(TAG, "onRefresh called from SwipeRefreshLayout");
                        fetchData();
                    }
                }
        );

        if(MyInstaGallery.networkAvailable()) {
            fetchData();
        } else {
            Toast.makeText(FeedActivity.this, getString(R.string.network_not_available), Toast.LENGTH_LONG).show();
        }
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void fetchData() {
        Call<InstagramResponse> call = RestClient.getRestService().getUserMedia(accessToken);
        call.enqueue(new Callback<InstagramResponse>() {
            @Override
            public void onResponse(@NonNull Call<InstagramResponse> call, @NonNull Response<InstagramResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Log.d(TAG, "Response successful");
                    Data[] fetchedData = response.body().getData();
                    if (fetchedData != null && fetchedData.length > 0) {
                        urlImages.clear();
                        for(Data data : fetchedData){
                            String url = data.getImages().getStandard_resolution().getUrl();
                            urlImages.add(url);
                        }

                        Log.d(TAG, "Fetched data count: " + fetchedData.length);
                        listViewAdapter.clearListView();
                        data.clear();
                        Collections.addAll(data, fetchedData);
                        listViewAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.d(TAG, "Response failed with code: " + response.code() + " and body:" + response.body());
                    Toast.makeText(getApplicationContext(), getString(R.string.network_error), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<InstagramResponse> call, @NonNull Throwable t) {
                //Handle failure
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
            }
        });
        swipeRefreshLayout.setRefreshing(false);
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }
}