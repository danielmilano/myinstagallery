package daniel.myinstagallery.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import daniel.myinstagallery.R;
import daniel.myinstagallery.listener.AuthenticationListener;

import static daniel.myinstagallery.MyInstaGallery.context;

/**
 * Created by Daniel on 05/11/2017.
 */

public class LoginActivity extends AppCompatActivity implements AuthenticationListener {

    private AuthenticationDialog authDialog;



    public static final String EXTRA_ACCESS_TOKEN = "accessToken";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        authDialog = new AuthenticationDialog(LoginActivity.this, LoginActivity.this);
        authDialog.setCancelable(true);
        authDialog.show();

        authDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });
    }

    @Override
    public void onCodeReceived(String accessToken) {
        if (accessToken == null) {
            Toast.makeText(context, getString(R.string.token_not_valid), Toast.LENGTH_SHORT).show();
        } else {
            authDialog.dismiss();
            Intent i = new Intent(LoginActivity.this, FeedActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.putExtra(EXTRA_ACCESS_TOKEN, accessToken);
            startActivity(i);

        }
    }

}
