package daniel.myinstagallery.utils;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Random;

import daniel.myinstagallery.R;

import static android.content.ContentValues.TAG;
import static daniel.myinstagallery.MyInstaGallery.context;

/**
 * Created by Daniel on 06/11/2017.
 */

public class FileUtils {

    public static void saveImage(String url){
        final URL imageUrl;
        final Bitmap bitmap;

        try {
            imageUrl = new URL(url);
            bitmap = BitmapFactory.decodeStream(imageUrl.openConnection().getInputStream());
        } catch (IOException e) {
            Log.e(TAG, "Error occurred while retrieving image url");
            Toast.makeText(context, context.getString(R.string.network_error), Toast.LENGTH_SHORT).show();
            return;
        }

        int n = 10000;
        n = new Random().nextInt(n);
        String filename = "Image-"+ n +".jpg";

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File f = new File(Environment.getExternalStorageDirectory() + File.separator + filename);
        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            Toast.makeText(context, String.format(context.getString(R.string.image_saved), f.getAbsolutePath()), Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Log.e(TAG, "Error occurred while creating file to share");
        }
    }

    public static Intent shareImage(String url) {
        final URL imageUrl;
        final Bitmap bitmap;
        try {
            imageUrl = new URL(url);
            bitmap = BitmapFactory.decodeStream(imageUrl.openConnection().getInputStream());
        } catch (IOException e) {
            Log.e(TAG, "Error occurred while retrieving image url");
            Toast.makeText(context, context.getString(R.string.network_error), Toast.LENGTH_SHORT).show();
            return null;
        }

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg";
        File f = new File(path);
        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
        } catch (IOException e) {
            Log.e(TAG, "Error occurred while creating file to share");
        }
        share.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
        return share;
    }

}
